import React, { Component } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Login from './components/views/login';
import Inicio from './components/views/inicio'
import Lista from './components/views/list'
import User from './components/views/user'
import Details from './components/views/details';

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

function DetailsScreen ({route, navigation}) {
  const {name} = route.params;
  const {genre} = route.params;
  const {auth} = route.params;
  const {description} = route.params;
  const {img} = route.params;
  const {img2} = route.params;

  const nombre = name
  const genero = genre
  const autor = auth
  const descripcion = description
  const imagen = img
  const imagen2 = img2

  return(
    <View>
      <Details 
        nombre={nombre}
        genero={genero}
        autor={autor}
        descripcion={descripcion}
        imagen={imagen}
        imagen2={imagen2}
        navigation={navigation}>
      </Details>
    </View>
  )
}


function TabMenu({navigation}) {
  return (
    <NavigationContainer independent={true}>
      <Tab.Navigator      
        activeColor="white"
        color={"gray"}
        barStyle={{backgroundColor: '#2F2F2F'}}
      >
        <Tab.Screen 
          name="Inicio"
          component={Inicio}
          options={{tabBarLabel: 'Inicio', tabBarIcon:({color}) =>
          (<MaterialCommunityIcons name="ballot" color={color} size={25} />)}}
        />        
        <Tab.Screen
          name="Lista"
          component={Lista}
          options={{tabBarLabel: 'Mangas', tabBarIcon:({color}) =>
          (<MaterialCommunityIcons name="alpha-m-box" color={color} size={25} />)}}
        />
        <Tab.Screen
          name="User"
          component={User}
          options={{tabBarLabel: 'Yo', tabBarIcon:({color}) =>
          (<MaterialCommunityIcons name="account" color={color} size={25} />)}}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="TabMenu"
            component={TabMenu}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="DetailsScreen"
            component={DetailsScreen}
            options={{headerShown: false}}
          />
          
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
