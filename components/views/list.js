import React, { Component } from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {Text, List, Avatar} from 'react-native-paper';
import {ScrollView} from 'react-native-gesture-handler';



function Lista({navigation}) {
        return (
        <View>
          <Text style={styles.head}>Lista de Mangas</Text>
          <Text style={styles.space}></Text>
            <ScrollView style={{backgroundColor: '#767676'}}>              
              <View style= {styles.container}>
                <View style={{flex:1, flexDirection: 'row'}}>
                  <Image style={styles.imageView} source = {require('../img/jojos.png')} />
                    <View style= {styles.textView}>
                      <Text style= {styles.title}>Titulo</Text>
                    </View>
                </View>
              </View>
            </ScrollView>
        </View>
    );
}


const styles = StyleSheet.create({
  loading: {
      flex: 1,
      padding:50,
      justifyContent: 'center',
      alignContent: 'center',
    },
  head: {
      fontSize: 25,
      textAlign: 'center',
      backgroundColor: '#2F2F2F',
      color: 'white',
  },
  container: {
    flex: 1,
    marginTop: 15,
    margin: 3,
    backgroundColor: '#767676',
  },
  imageView: {

      width: '30%',
      height: 90 ,
      margin: 20,
      borderRadius : 30, 
  },
  textView: {
      width:'45%', 
      textAlignVertical:'center',
      textAlign:'justify',
      justifyContent: 'center',
      fontFamily: 'Pacifico-Regular',
  },
  textvolver:{
      width: '25%',
      justifyContent: 'center'
      
  },
  item: {
    backgroundColor: 'gray',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    fontSize: 20,
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontFamily: 'Bangers',
  },
  space: {
    backgroundColor: 'black',
    fontSize: 2,
  },
  titleloading: {
      fontSize: 40,
      color: '#c43c00',
      fontWeight: 'bold',
      fontFamily: 'Bangers',
    },
});
export default Lista;