import React, { Component } from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {Text, Searchbar, Card, Title, Paragraph} from 'react-native-paper';
import {ScrollView} from 'react-native-gesture-handler';

export default class Details extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return(
            <View style={styles.container}>
                <Text style={styles.title}>{this.props.nombre}</Text>
                <Image source={{uri: img}}/>
                <Text>Descripcion</Text>
                <Text>{this.props.descripcion}</Text>
                <Text>{this.props.genero}</Text>
                <Text>{this.props.autor}</Text>
                <Text>Contenido</Text>
                <Text>{this.props.descripcion}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        alignItems: 'center',
        color: 'blue',
        padding: 10,
    },
    button: {
        top: 10,
        alignItems: 'center',
        backgroundColor: 'blue',
        padding: 10,
    },
    item: {
        backgroundColor: 'white',
        marginTop: 20,
        marginHorizontal: 20,
        borderColor: 'orange',
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 10,

    },
    head: {
        fontSize: 32,
        textAlign: 'center',
        color: 'white',
        backgroundColor: 'orange'
    },
    subtitle: {
        fontSize: 20,
        textAlign: 'center',
        backgroundColor: 'black',
        color: 'white'
    },
    title: {
        fontSize: 30,
        textAlign: 'center',
        backgroundColor: 'red',
        color: 'white',
    },
    image: {
        width: 'auto',
        height: 200,
    },
    space: {
        backgroundColor: 'black',
        fontSize: 2,
    }
});
