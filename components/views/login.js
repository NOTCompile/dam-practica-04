import React, { Component } from 'react';

import {Button, TextInput} from 'react-native-paper';

import { View, StyleSheet, Image } from 'react-native';

import { Text, Input} from 'react-native-elements';

import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            textUser: '',
            textPass: '',
            error: null,
        };
    }

    changeTextUser = (text) => {
        this.setState({textUser: text});
    }

    changeTextPass = (text) => {
        this.setState({textPass: text});
    }

    render() {
        const {navigation} = this.props;

        return (
            <View style={styles.container}>
                <Text h2>Iniciar Sesion</Text>
                <Image source={require('../img/login.png')}
                    style={styles.image} />
                <Input
                    placeholder='Usuario'
                    leftIcon={{ type: 'font-awesome', name: 'user', size: 40 }}
                />
                <Input
                    placeholder='Contraseña'
                    leftIcon={{ type: 'font-awesome', name: 'lock', size: 40 }}
                />
                <Button
                    icon= "arrow-right-bold-box-outline"
                    mode="contained"
                    onPress={() => {navigation.navigate('TabMenu')} }
                >
                    Ingresar
                </Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#E1E1E1',
        justifyContent: 'center',
    },
    text: {
        alignItems:'center',
        padding: 10,
    },
    image: {
        width: 200,
        height: 200,
        
    }
});

export default Login;